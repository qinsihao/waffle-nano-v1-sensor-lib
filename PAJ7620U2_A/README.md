# PAJ7620U2手势识别传感器

## 案例展示

![up_img](/PAJ7620U2_/PAJ7620U2/img/up_img.jpg)

## 物理连接

### 传感器选择

&emsp;&emsp;本项目选择下方型号为PAJ7620U2的手势传感器模组

&emsp;&emsp;![PAJ7620U2_img](/PAJ7620U2_/PAJ7620U2/img/PAJ7620U2_img.jpg)

### 传感器接线

&emsp;&emsp;传感器接线如下表所示

| Waffle Nano | PAJ7620U2 |
| ----------- | --------- |
| 3V3         | VCC       |
| GND         | GND       |
| G01         | SCL       |
| G00         | SDA       |

## 传感器库

可在主函数中通过以下代码块将库导入

``
from PAJ7620U2 import PAJ7620U2
``

### 类

#### PAJ7620U2(i2c)

&emsp;&emsp;i2c是已经构造好的对象

```python
from machine import I2C  

i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000)  

g=PAJ7620U2(i2c)  
```

### 函数

&emsp;&emsp;下面列举传感器库中所使用的函数

#### 寄存器初始化

&emsp;&emsp;`__init__(self, i2c)`

&emsp;&emsp;函数说明：将寄存器初始化，返回"PAJ7620U2 initialize register finished."表示初始化成功

#### 写入寄存器数据

&emsp;&emsp;`PAJ7620U2WriteReg(self, register, data)`  
&emsp;&emsp;函数说明：将一个字节写入手势传感器上的寄存器  
&emsp;&emsp;`register`::操作的起始寄存器地址  
&emsp;&emsp;`data`:需要写入（发送）的数据

#### 选择寄存器分块

&emsp;&emsp;`PAJ7620U2SelectBank(self, bank)`  
&emsp;&emsp;函数说明：选择手势传感器上的寄存器分块  
&emsp;&emsp;`bank`:寄存器分块，本库分为0和1

#### 读取寄存器数据

&emsp;&emsp;`PAJ7620U2ReadReg(self, register, length)`  
&emsp;&emsp;函数说明：从手势传感器的地址“register”开始读取长度为“length”的字节块  
&emsp;&emsp;`register`::操作的起始寄存器地址  
&emsp;&emsp;`data`:需要读取的数据长度

#### 打印传感器的值

&emsp;&emsp;`print_gesture(self)`  
&emsp;&emsp;函数说明：打印手势传感器的值

#### 获取手势数据

&emsp;&emsp;`return_gesture(self)`  
&emsp;&emsp;函数说明：返回传感器检测到的数据

返回值：

&emsp;&emsp;向上`self.GES_UP_FLAG`->`1`  
&emsp;&emsp;向下`self.GES_DOWN_FLAG`->`2`  
&emsp;&emsp;向左`self.GES_LEFT_FLAG`->`4`  
&emsp;&emsp;向右`self.GES_RIGHT_FLAG`->`8`  
&emsp;&emsp;向前`self.GES_FORWARD_FLAG`->`16`  
&emsp;&emsp;向后`self.GES_BACKWARD_FLAG`->`32`  
&emsp;&emsp;顺时针`self.GES_CLOCKWISE_FLAG`->`64`  
&emsp;&emsp;逆时针`self.GES_COUNT_CLOCKWISE_FLAG`->`128`  

&emsp;&emsp;关于此库相关细节说明详见 **代码注释**

## 案例代码复现

&emsp;&emsp;可以获取PAJ7620U2/code/PAJ7620U2.py函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例
