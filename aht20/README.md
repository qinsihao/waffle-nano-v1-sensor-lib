#  AHT20

用于AHT20和AHT10温度和湿度传感器的MicroPython驱动程序。



## 案例展示

  读取温湿度数据, 并判断温度与湿度的高低情况，将其输出在屏幕上

![1.jpg](/aht20/img/1.jpg)

## 物理连接

 SCL --> G01
 SDA --> G00
 VCC --> 3V3
 GND --> GND

### 传感器选择

   传感器选择如下图所示的型号为aht20的温湿度传感器模块

![2.jpg](/aht20/img/2.jpg)

### 

## 传感器库使用

  可以获取[ahtx0.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master//aht20/code/ahtx0.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`

  我们在可以在主函数中使用以下代码导入此库

```python
import ahtx0
```

  在对象构造函数中，我们需要传入三个引脚数字，分别是ADC引脚，以及两个可设置模式为输入的引脚

```python
heartSensor = AD8232(analogPin = 5, LO1Pin = 2, LO2Pin = 14) #构造心电传感器对象
```

  使用心电传感器对象的`read()`方法读取出整型数据

```
x=int(sensor.temperature*100) x=int(sensor.relative_humidity*100) #从心电传感器中获取数据
```

​		关于此库相关细节说明详见代码注释

# 样例代码

import utime
from machine import Pin, I2C

import ahtx0
i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000)

#使用I2C创建传感器对象

sensor = ahtx0.AHT20(i2c)

while True:
    print("\nTemperature: %0.2f C" % sensor.temperature)
    print("Humidity: %0.2f %%" % sensor.relative_humidity)
    utime.sleep(5)

# 使用I2C创建传感器对象
sensor = ahtx0.AHT20(i2c)

while True:
    print("\nTemperature: %0.2f C" % sensor.temperature)
    print("Humidity: %0.2f %%" % sensor.relative_humidity)
    utime.sleep(5)

## 案例代码复现

  可以获取[main.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/aht20/code/main.py)函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例。

  案例相关细节说明详见代码注释

