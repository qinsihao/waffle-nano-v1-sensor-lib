from machine import Pin,SPI
from machine import Pin,ADC
from heart import init,screen_begin,screen_form
from heart import screen_little_heart_1,screen_little_heart_2
from MQTT import MQTTClient
import utime, machine, network
import st7789
import utime

global Mymsg
mymsg=None

pinLo1 = Pin(2, Pin.IN)
pinLo2 = Pin(14, Pin.IN)  # 信号输入
adc = ADC(Pin(5))  # ADC设置
adc.equ(ADC.EQU_MODEL_8)  # 采样8次

wl = network.WLAN(network.STA_IF)
wl.active(1)
wl.connect('123','12345678',security=network.AUTH_PSK)

client_id = "www"
mqtt_server = "thingworx.zucc.edu.cn"
port=1883
topic_sub = "wsy"
topic_pub = "wsy"
mqtt_user = ""
mqtt_password = "" 

def sub_cb(topic, msg):
    global mymsg
    mymsg=msg
    #print((topic, msg))
    #client.publish(topic_pub, msg)

def connect_and_subscribe():
    global client_id, mqtt_server, topic_sub, mqtt_user, mqtt_password
    client = MQTTClient(client_id, mqtt_server, user=mqtt_user, password=mqtt_password,port=port)
    client.set_callback(sub_cb)
    client.connect()
    client.subscribe(topic_sub)
    print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
    return client

def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    utime.sleep(10)
    machine.reset()

try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()

display = init()#屏幕初始化

screen_begin(display)  

while True:
    client.subscribe(topic_sub)
    if mymsg!=None:
        Mymsg=str(mymsg,'utf-8')
        print(Mymsg)
        if (Mymsg=='start'):
            display.fill(st7789.color565(255,255,255))
            x1=0
            y1=80
            sum_adc=0
            i=0
            start = utime.ticks_ms()#开始时间为毫秒的递增计数器
            x=0
            y=0
            data=600
            average=0
            t=0
            screen_form(display)

            while True:
                if(pinLo1.value == 1 or pinLo2.value == 1):
                    display.draw_string(20,40,"Your patch to fall off",size=3,color=st7789.color565(0,0,0))
                elif(utime.ticks_diff(utime.ticks_ms(),start) == 60000):
                    utime.sleep(2)
                    break#一分钟结束测试
                else:
                    
                    x2=x1+1
                    y2=240-int(adc.read()/10)
                    display.line(x1, y1, x2, y2, st7789.color565(0,0,0))
                    x1=x1+1
                    y1=y2
                    value=adc.read()-data
                    data=adc.read()
                    if(value>=400):
                        sum_adc=sum_adc+value
                        i=i+1
                        average=int(sum_adc/i/10*1.5)#心率数据
                        t=1
                    if(x2 == 240 or x1 == 239):
                        display.fill(st7789.color565(255,255,255))
                        
                        display.draw_string(150,33,'0'+str(average),size=4,color=st7789.color565(0,0,0))
                        x1=0#清屏重置图形数据
                        screen_form(display)
                        if(average==0):
                            display.draw_string(20,80,"Your patch",size=4,color=st7789.color565(0,0,0))
                            display.draw_string(0,120,"to fall off!",size=4,color=st7789.color565(0,0,0))
                            continue
            
                utime.sleep(0.01)
    
            display.fill(st7789.color565(255,255,255))
            display.draw_string(40,50,"End of the test!",size=2,color=st7789.color565(255,0,0))
            display.draw_string(50,100,"The result has",size=2,color=st7789.color565(0,0,0))
            display.draw_string(38,120,"been sent to your",size=2,color=st7789.color565(0,0,0))
            display.draw_string(60,140,"mobile phone",size=2,color=st7789.color565(0,0,0))
            utime.sleep(3)
            display.fill(st7789.color565(255,255,255))
            utime.sleep(0.1)
            screen_little_heart_1(display)
            display.draw_string(50,105,"Thanks!",size=4,color=st7789.color565(0,0,255))
            screen_little_heart_2(display)    
        
        client.publish(topic_pub,"Heart Rate: "+str(average))#返还数据
        utime.sleep_ms(500)
        client.subscribe(topic_sub)
        mymsg=None #结束循环发送信息