# MAX30102

## 案例展示

利用传感器测定手指或者手腕处的心率，结果如下图：

![](image/9e86b6b10f338ac1d1086343fd7821c.jpg)

## 物理连接

### 传感器选择

   传感器选择如下图所示的型号为MAX30102心率传感器模组。

![](image/0028943a3031287925cbe8d5ebf1849.jpg)

![](image/1758e1ea4867d413c92b94985e996c7.jpg)

### 传感器接线

  传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3.3         | VIN    |
| IO0         | SDA    |
| IO1         | SCL    |
| GND         | GND    |

## 传感器库使用

  可以获取[max30102.py](https://gitee.com/yin-huaichang/waffle-nano-v1-sensor-lib/blob/master/MAX30102/code/max30102.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到Waffle Nano上。

  我们在可以在主函数中使用以下代码导入此库。

```
import max30102
```

  在对象构造函数中，我们需要传入一个已经构造好的`IIC`对象

```
# 此处省略IIC对象的构造过程
m = max30102.MAX30102(0,1,100)#构造心率对象
```

  我们使用心率传感器对象的`read()`方法读取出一个寄存器中的red红灯数据。

```
red = m.read_fifo() #从传感器中获取红灯寄存器数据
```

  关于此库相关细节说明详见代码注释

## 案例代码复现

  可以获取[main.py](https://gitee.com/yin-huaichang/waffle-nano-v1-sensor-lib/blob/master/MAX30102/code/main.py)代码，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给Waffle Nano，以复现此案例。

  案例相关细节说明详见代码注释