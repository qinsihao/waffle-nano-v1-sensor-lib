from machine import I2C, Pin, SPI
import utime
import ahtx0
import st7789
from turtle_nano import Turtle
i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000)
print(i2c.scan())
sensor = ahtx0.AHT20(i2c)
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))

display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.fill(st7789.color565(0, 0, 0))
pic = Turtle(display,200, 150,st7789.color565(0, 0, 0))
while (1):
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~绘制温度计
    
    pic.pencolor(st7789.color565(0, 0, 0))
    pic.forward(10)
    # 右转90度:
    pic.right(90)

    # 修改笔刷颜色:
    pic.pencolor(st7789.color565(255, 255, 255))
    pic.forward(90)
    pic.right(90)

    pic.pencolor(st7789.color565(255, 255, 255))
    pic.forward(10)
    pic.right(90)

    pic.pencolor(st7789.color565(255, 255, 255))
    pic.forward(90)
    pic.right(135)
    pic.circle(9)
    pic.pencolor(st7789.color565(0, 255, 0))
    i=0
    while i<=9:
        i=i+0.2
        pic.circle(i)
    pic.left(45)
    i=0
    k1=sensor.temperature
    
    t=int(k1)
    while t>=-25:
        pic.pencolor(st7789.color565(i,255-i,0))
        pic.forward(10)
        pic.right(90)
        pic.forward(1)
        pic.right(90)
        pic.forward(10)
        pic.left(180)
        i=i+4
        t=t-1
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    pic.penup()
    pic.goto(0,0)
    pic.pendown()
    a=int(k1*100)
    b=int(a/100)
    c=int(a%100)
    if a ==-5000 :#根据公式，当无输入时计算得出的a即为-5000
     display.draw_string(0, 10,"Check the link",size=2,color=st7789.color565(255, 255, 255))
    elif a>-5000 and a<-4500 :
     display.draw_string(0, 10,"Temperature is too low!",size=2,color=st7789.color565(255, 255, 255))
    elif a>=8500 :
     display.draw_string(0, 10,"Temperature is too high!",size=2,color=st7789.color565(255, 255, 255))
    elif a>=-4500 and a<0 :
     print(c)       
     display.draw_string(0, 10, "Temperature:"+str(b)+"."+str(100-c)+" C",size=2,color=st7789.color565(255, 255, 255))
    else :
     display.draw_string(0, 10, "Temperature:"+str(b)+"."+str(c)+" C",size=2,color=st7789.color565(255, 255, 255))
    display.draw_string(0, 30,"Humidity:   %0.2f %%" % sensor.relative_humidity,size=2,color=st7789.color565(255, 255, 255))
 
    utime.sleep(4)
    display.fill_rect(120,0,120,60,st7789.color565(0,0,0))
    display.fill_rect(201,58,9,90,st7789.color565(0,0,0))