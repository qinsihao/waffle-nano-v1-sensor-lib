# HDC1080温湿度传感器

## 案例展示

![simple_img](https://gitee.com/milkymate/waffle-nano-v1-sensor-lib/raw/master/HDC1080/img/simple_img.jpg)

## 物理连接

### 传感器选择

&emsp;&emsp;本项目选择下方型号为HDC1080的温湿度传感器模组

&emsp;&emsp;![HDC1080](https://gitee.com/milkymate/waffle-nano-v1-sensor-lib/raw/master/HDC1080/img/HDC1080.jpg)

### 传感器接线

&emsp;&emsp;传感器接线如下表所示

| Waffle Nano | HDC1080 |
| ----------- | ------- |
| G00         | SDA     |
| G01         | SCL     |
| GND         | GND     |
| 3V3         | VCC     |

## 传感器库

可在主函数中通过以下代码块将库导入

``
import HDC1080
``

### 类

#### 构造I2C对象

```python
i2c = I2C(1,sda=Pin(0),scl=Pin(1))
hdc1080 = HDC1080.HDC1080(i2c)
```

### 函数

&emsp;&emsp;下面列举传感器库中所使用的函数

#### 寄存器初始化

&emsp;&emsp;`__init__(self, i2c)`

&emsp;&emsp;函数说明：初始化寄存器，发出开始测量指令

#### 读取温度数据

&emsp;&emsp;`readTemperature(self)`

&emsp;&emsp;函数说明：读取传感器所收集的温度数据并返回

#### 读取湿度数据

&emsp;&emsp;`readHumidity(self)`

&emsp;&emsp;函数说明：读取传感器所收集的湿度数据并返回

#### 字节转换

&emsp;&emsp;`bytesToInt2(bytes)`

&emsp;&emsp;函数说明：将字节数据转换为整型数据

## 传感器寄存器地址

### 寄存器

```python
HDC1080_TEMPERATURE_REGISTER = (0x00)
HDC1080_HUMIDITY_REGISTER = (0x01)
HDC1080_CONFIGURATION_REGISTER = (0x02)
```

### 配置寄存器

```python
HDC1080_CONFIG_RESET_BIT = (0x8000)
HDC1080_CONFIG_HEATER_ENABLE = (0x2000)
HDC1080_CONFIG_ACQUISITION_MODE = (0x1000)
```

### 分辨率

#### 温度分辨率

```python
HDC1080_CONFIG_TEMPERATURE_RESOLUTION_14BIT = (0x0000)
HDC1080_CONFIG_TEMPERATURE_RESOLUTION_11BIT = (0x0400)
```

#### 湿度分辨率

```python
HDC1080_CONFIG_HUMIDITY_RESOLUTION_14BIT = (0x0000)
HDC1080_CONFIG_HUMIDITY_RESOLUTION_11BIT = (0x0100)
HDC1080_CONFIG_HUMIDITY_RESOLUTION_8BIT = (0x0200)
```

&emsp;&emsp;关于此库相关细节说明详见 **代码注释**

&emsp;&emsp;可以获取HDC1080/code/HDC1080.py函数和HDC1080/code/main.py函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传给`Waffle Nano`，以复现此案例。
