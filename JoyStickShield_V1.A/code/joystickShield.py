from machine import ADC, Pin


# Collect data to virables.


class Cmd:
    def __init__(self):
        self.x = ADC(Pin(12))
        self.y = ADC(Pin(13))
        self.a = Pin(0, Pin.IN)
        self.b = Pin(1, Pin.IN)
        self.c = Pin(5, Pin.IN)
        self.d = Pin(14, Pin.IN)

    def statusA(self):
        return self.a.value()

    def statusB(self):
        return self.b.value()

    def statusC(self):
        return self.c.value()

    def statusD(self):
        return self.d.value()

# Buttons. 1=Idle, 0=Pressed

    def statusX(self):
        if self.x.read() <= 700:
            return -1
        if self.x.read() >= 1300:
            return 1
        return 0

    def statusY(self):
        if self.y.read() <= 700:
            return -1
        if self.y.read() >= 1300:
            return 1
        return 0

# Sticks. -1=Negative position, 0=Idle, 1=Positive position

    def exactX(self):
        return self.x.read()

    def exactY(self):
        return self.y.read()

# Get the exact value
