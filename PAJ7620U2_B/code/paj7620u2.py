from utime import sleep_ms


# 定义函数
def bit(data):
    return 1 << data


def delay(time):
    sleep_ms(time)


# 定义PAJ7620U2类
class PAJ7620U2:
    # 初值
    # 寄存器地址
    bank_register = 0xEF
    switch_register = 0x41
    gesture_register = 0x43
    proximity_register = 0x44

    # PAJ7620U2模式
    gesture_mode = 0
    proximity_mode = 1
    suspend_mode = 2
    resume_mode = 3

    recent_mode = 0

    # 手势参数
    up = bit(0)
    down = bit(1)
    left = bit(2)
    right = bit(3)
    forward = bit(4)
    backward = bit(5)
    clockwise = bit(6)
    counterclockwise = bit(7)
    wave = bit(8)
    _wave = bit(0)

    leave = 0
    approach = bit(0)

    recent_gesture = 0

    # 延迟设置
    detection_delay = 50
    quit_delay = 450

    # 传感器初始化数值
    initialization_register_array = ([0xEF, 0x00],
                                     [0x37, 0x07],
                                     [0x38, 0x17],
                                     [0x39, 0x06],
                                     [0x42, 0x01],
                                     [0x46, 0x2D],
                                     [0x47, 0x0F],
                                     [0x48, 0x3C],
                                     [0x49, 0x00],
                                     [0x4A, 0x1E],
                                     [0x4C, 0x20],
                                     [0x51, 0x10],
                                     [0x5E, 0x10],
                                     [0x60, 0x27],
                                     [0x80, 0x42],
                                     [0x81, 0x44],
                                     [0x82, 0x04],
                                     [0x8B, 0x01],
                                     [0x90, 0x06],
                                     [0x95, 0x0A],
                                     [0x96, 0x0C],
                                     [0x97, 0x05],
                                     [0x9A, 0x14],
                                     [0x9C, 0x3F],
                                     [0xA5, 0x19],
                                     [0xCC, 0x19],
                                     [0xCD, 0x0B],
                                     [0xCE, 0x13],
                                     [0xCF, 0x64],
                                     [0xD0, 0x21],
                                     [0xEF, 0x01],
                                     [0x02, 0x0F],
                                     [0x03, 0x10],
                                     [0x04, 0x02],
                                     [0x25, 0x01],
                                     [0x27, 0x39],
                                     [0x28, 0x7F],
                                     [0x29, 0x08],
                                     [0x3E, 0xFF],
                                     [0x5E, 0x3D],
                                     [0x65, 0x96],
                                     [0x67, 0x97],
                                     [0x69, 0xCD],
                                     [0x6A, 0x01],
                                     [0x6D, 0x2C],
                                     [0x6E, 0x01],
                                     [0x72, 0x01],
                                     [0x73, 0x35],
                                     [0x74, 0x00],
                                     [0x77, 0x01])

    # 手势模式数据
    gesture_register_array = ([0xEF, 0x00],
                              [0x41, 0x00],
                              [0x42, 0x00],
                              [0xEF, 0x00],
                              [0x48, 0x3C],
                              [0x49, 0x00],
                              [0x51, 0x10],
                              [0x83, 0x20],
                              [0x9f, 0xf9],
                              [0xEF, 0x01],
                              [0x01, 0x1E],
                              [0x02, 0x0F],
                              [0x03, 0x10],
                              [0x04, 0x02],
                              [0x41, 0x40],
                              [0x43, 0x30],
                              [0x65, 0x96],
                              [0x66, 0x00],
                              [0x67, 0x97],
                              [0x68, 0x01],
                              [0x69, 0xCD],
                              [0x6A, 0x01],
                              [0x6b, 0xb0],
                              [0x6c, 0x04],
                              [0x6D, 0x2C],
                              [0x6E, 0x01],
                              [0x74, 0x00],
                              [0xEF, 0x00],
                              [0x41, 0xFF],
                              [0x42, 0x01])

    # 接近模式数据
    proximity_register_array = ([0xEF, 0x00],
                                [0x41, 0x00],
                                [0x42, 0x02],
                                [0x48, 0x20],
                                [0x49, 0x00],
                                [0x51, 0x13],
                                [0x83, 0x00],
                                [0x9F, 0xF8],
                                [0x69, 0x96],
                                [0x6A, 0x02],
                                [0xEF, 0x01],
                                [0x01, 0x1E],
                                [0x02, 0x0F],
                                [0x03, 0x10],
                                [0x04, 0x02],
                                [0x41, 0x50],
                                [0x43, 0x34],
                                [0x65, 0xCE],
                                [0x66, 0x0B],
                                [0x67, 0xCE],
                                [0x68, 0x0B],
                                [0x69, 0xE9],
                                [0x6A, 0x05],
                                [0x6B, 0x50],
                                [0x6C, 0xC3],
                                [0x6D, 0x50],
                                [0x6E, 0xC3],
                                [0x74, 0x05])

    # 挂起模式数据
    suspend_register_array = ([0xEF, 0x01],
                              [0x72, 0x00],
                              [0xEF, 0x00],
                              [0x03, 0x01])

    # 恢复模式数据
    resume_register_array = ([0xEF, 0x01],
                             [0x72, 0x01])

    # 初始化
    def __init__(this, i2c, address):
        delay(700)  # 等待PAJ7620U2稳定

        this.i2c = i2c
        this.address = address  # 设置需要通信的从机地址

        this.select_bank(0)

        initialization_status = this.read(0x00, 1)[0]
        if initialization_status == 0x20:  # 检测是否被唤醒
            print("Wake up finished.")
        else:
            print("Wake up failed.")
            return

        for item in this.initialization_register_array:  # 写入初始数据
            this.write(item[0], item[1])

        this.select_bank(0)

        this.change_mode(this.gesture_mode)
        print("Initialization finished.")

    # 读取寄存器数据
    def read(this, register, length):
        return this.i2c.readfrom_mem(this.address, register, length)

    # 写入寄存器数据
    def write(this, register, data):
        return this.i2c.writeto_mem(this.address, register, bytes([data]))
        # return this.i2c.write(this.address, bytes([register, data])) # 若writeto_mem方法失效则启用该行

    # 寄存器分块选择
    def select_bank(this, bank):
        this.write(this.bank_register, bank)

    # 传感器模式选择
    def change_mode(this, mode):
        if mode == this.gesture_mode:  # 手势模式
            for item in this.gesture_register_array:
                this.write(item[0], item[1])

            this.select_bank(0)

        elif mode == this.proximity_mode:  # 接近模式
            for item in this.proximity_register_array:
                this.write(item[0], item[1])

            this.select_bank(0)

        elif mode == this.suspend_mode:  # 挂起模式
            for item in this.suspend_register_array:
                this.write(item[0], item[1])

            this.select_bank(0)

        elif mode == this.resume_mode:  # 恢复模式
            for item in this.resume_register_array:
                this.write(item[0], item[1])

            this.select_bank(0)

    # 手势检测开关
    def gesture_switch(this, up=True, down=True, left=True, right=True, forward=True, backward=True, clockwise=True, counterclockwise=True, wave=True):
        switch = 0

        switch += this.up*up
        switch += this.down*down
        switch += this.left*left
        switch += this.right*right
        switch += this.forward*forward
        switch += this.backward*backward
        switch += this.clockwise*clockwise
        switch += this.counterclockwise*counterclockwise
        this.write(this.switch_register, switch)

        switch += this._wave*wave
        this.write(this.switch_register+1, switch)

    # 获取手势数据
    def get_gesture(this):
        if this.recent_mode == this.gesture_mode:  # 手势模式
            data = this.read(this.gesture_register, 1)[0]  # 获取原始数据

            if data:
                return data

            else:
                data = this.read(this.gesture_register+1, 1)[0]
                if data:
                    return this.wave

        elif this.recent_mode == this.proximity_mode:  # 接近模式
            data = this.read(this.proximity_register, 1)[0]  # 获取原始数据

            if data != this.recent_gesture:
                this.recent_gesture = data

                if data == this.approach:
                    return this.approach
                else:
                    return this.leave
