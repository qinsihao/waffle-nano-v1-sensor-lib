# 缺省库
from machine import I2C, Pin, SPI
from st7789 import ST7789

# 导入库
from gluttonous_snake import GluttonousSnake
from paj7620u2 import PAJ7620U2, delay


# 构建SPI对象
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))

# 构建ST7789对象
screen = ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))

# 构建I2C对象
i2c = I2C(1, sda=Pin(0), scl=Pin(1))
address = i2c.scan()[0]

# 构建PAJ7620U2对象
sensor = PAJ7620U2(i2c, address)
sensor.gesture_switch(forward=False, backward=False, wave=False)

# 构建GluttonousSnake对象
game = GluttonousSnake(screen, sensor,snake_speed=1)

# 开始游戏
game.start()

end = False
while not end:
    data = sensor.get_gesture()

    if data == sensor.clockwise:  # 开始游戏
        game.init()
        print("Game Start.")
        result = game.play()

        if not result:
            delay(3000)
            game.restart()

            delay(100)
            while True:
                data = sensor.get_gesture()

                if data == sensor.clockwise:
                    break
                elif data == sensor.counterclockwise:
                    screen.fill(0x0000)
                    print("Game Over.")
                    end = True