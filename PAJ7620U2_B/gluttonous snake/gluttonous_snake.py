from utime import sleep_ms
from board import Board
from element import Snake
from board import Board


# 定义函数
def delay(time):
    sleep_ms(time)


# 定义GluttonousSnake类
class GluttonousSnake:
    def __init__(this, screen, gesture, snake_length=3, snake_speed=2):
        this.draw = Board(screen)
        this.s = Snake((0, this.draw.grid_number-1), snake_length, snake_speed)

        this.gesture = gesture

    def start(this):
        this.draw.new()

        this.draw.string(15, 30, "Clockwise", 4, 4)
        this.draw.string(95, 100, "To", 4, 4)
        this.draw.string(60, 170, "Start", 4, 4)

    def restart(this):
        this.draw.new()

        this.draw.string(15, 5, "Clockwise", 3, 10)
        this.draw.string(100, 35, "To", 3, 10)
        this.draw.string(35, 65, "Restart", 3, 10)

        this.draw.string(100, 95, "OR", 3, 10)

        this.draw.string(50, 125, "Couter", 3, 10)
        this.draw.string(15, 155, "Clockwise", 3, 10)
        this.draw.string(100, 185, "To", 3, 10)
        this.draw.string(75, 215, "Exit", 3, 10)

    def init(this):
        this.draw.new()
        this.s.new()

        for i in this.s.body.element:
            this.draw.point(i[0], i[1], this.s.body_color)

        this.draw.point(this.s.food.coordinate[0], this.s.food.coordinate[1], this.s.food.box_color)

    def go(this, direction):
        result = this.s.turn(direction)

        if result:
            this.draw.point(this.s.get_head()[0], this.s.get_head()[1], this.s.body_color)

            if not this.s.eat(this.s.get_head()):
                this.draw.point(this.s.get_tail()[0], this.s.get_tail()[1], this.draw.background_color)
                this.s.body.dequeue()
            else:
                this.draw.point(this.s.food.coordinate[0], this.s.food.coordinate[1], this.s.food.box_color)

            return True
        else:
            return False

    def play(this):
        timer = 0
        gesture = 0
        result = True

        while True:
            data = this.gesture.get_gesture()  # 获取手势数据

            if data:  # 保存手势数据
                print(data)
                gesture = data

            # 控制蛇移动速度
            if timer < 1000:
                timer += this.s.speed

            else:
                timer = 0

                if gesture:
                    if gesture == this.gesture.up or gesture == this.gesture.forward:
                        result = this.go(this.s.up)
                    elif gesture == this.gesture.down or gesture == this.gesture.backward:
                        result = this.go(this.s.down)
                    elif gesture == this.gesture.left:
                        result = this.go(this.s.left)
                    elif gesture == this.gesture.right:
                        result = this.go(this.s.right)

                    elif gesture == this.gesture.clockwise:
                        if this.s.direction == this.s.up:
                            result = this.go(this.s.right)
                            result = this.go(this.s.down)
                        elif this.s.direction == this.s.down:
                            result = this.go(this.s.left)
                            result = this.go(this.s.up)
                        elif this.s.direction == this.s.left:
                            result = this.go(this.s.up)
                            result = this.go(this.s.right)
                        elif this.s.direction == this.s.right:
                            result = this.go(this.s.down)
                            result = this.go(this.s.left)
                    elif gesture == this.gesture.counterclockwise:
                        if this.s.direction == this.s.up:
                            result = this.go(this.s.left)
                            result = this.go(this.s.down)
                        elif this.s.direction == this.s.down:
                            result = this.go(this.s.right)
                            result = this.go(this.s.up)
                        elif this.s.direction == this.s.left:
                            result = this.go(this.s.down)
                            result = this.go(this.s.right)
                        elif this.s.direction == this.s.right:
                            result = this.go(this.s.up)
                            result = this.go(this.s.left)

                    # 重置手势
                    gesture = 0

                else:
                    result = this.go(this.s.direction)

                if not result:  # 游戏失败
                    delay(1500)
                    this.draw.new()
                    this.draw.string(50, 50, "GAME", 4, 20)
                    this.draw.string(50, 150, "OVER", 4, 20)

                    delay(800)
                    this.draw.new()
                    this.draw.string(50, 40, "GAME", 4, 20)
                    this.draw.string(50, 140, "OVER", 4, 20)

                    delay(800)
                    this.draw.new()
                    this.draw.string(50, 30, "GAME", 4, 20)
                    this.draw.string(50, 130, "OVER", 4, 20)

                    this.draw.string(50, 185, "Your", 2, 16)
                    this.draw.string(50, 210, "Score", 2, 10)
                    this.draw.string(160, 195, str(this.s.score), 3, 5)

                    return False
