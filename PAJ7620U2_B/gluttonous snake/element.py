import urandom as nm
import utime as tm
from queue import Queue


# 定义Box类
class Box:
    # 颜色参数
    box_color = 0xf0f0

    # 坐标参数
    coordinate = (0, 0)

    def __init__(this, edge):
        this.edge = edge

    def new(this):
        nm.seed(tm.ticks_cpu())
        x = nm.randint(this.edge[0], this.edge[1])
        y = nm.randint(this.edge[0], this.edge[1])

        this.coordinate = (x, y)


# 定义Snake类
class Snake:
    # 移动方向
    up = (0, -1)
    down = (0, 1)
    left = (-1, 0)
    right = (1, 0)

    clockwise = -1
    counterclockwise = 1

    # 颜色参数
    body_color = 0xf7f5

    # 初始化
    def __init__(this, edge, length, speed):
        this.body = Queue(24*24)
        this.food = Box(edge)

        this.edge = edge
        this.speed = speed
        this.length = length

    def new(this):
        this.direction = this.right

        this.body.clear()
        for i in range(this.length):
            this.body.enqueue((i, 0))

        this.score = 0
        this.new_food()

    def get_head(this):
        return this.body.get_rear()

    def get_tail(this):
        return this.body.get_front()

    def turn(this, direction):
        if (direction[0], direction[1]) == (-this.direction[0], -this.direction[1]):  # 反向则不转弯
            direction = this.direction

        head = (this.get_head()[0]+direction[0], this.get_head()[1]+direction[1])

        if head[0] == this.edge[0]-1 or head[0] == this.edge[1]+1 or head[1] == this.edge[0]-1 or head[1] == this.edge[1]+1:  # 是否触碰边界
            print("Die in boundary.")
            return False

        if head in this.body.element:  # 是否触碰自身
            print("Die in this.")
            return False

        this.direction = direction
        this.body.enqueue(head)

        return True

    def eat(this, head):
        if this.food.coordinate == head:
            this.new_food()
            this.score += 1

            return True
        else:
            return False

    def new_food(this):
        while True:
            this.food.new()
            if this.food.coordinate not in this.body.element:
                break
