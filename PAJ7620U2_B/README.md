# 手势识别

## 案例展示

&emsp;&emsp;手势传感器检测动作手势，并输出手势方向。

![Sample](/PAJ7620U2_B/img/Sample.jpg)

## 物理连接

### 传感器选择

&emsp;&emsp;传感器选择如下图所示的型号为PAJ7620U2的手势传感器模组。

![PAJ7620U2](/PAJ7620U2_B/img/PAJ7620U2.jpg)

### 传感器接线

&emsp;&emsp;传感器与`Waffle Nano`之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连状态。

|Waffle Nano|传感器|
|---|---|
|3.3|VCC|
|IO0|SDA|
|IO1|SCL|
|GND|GND|

## 传感器库使用

&emsp;&emsp;可以获取[PAJ7620U2.py](/PAJ7620U2_B/code/paj7620u2.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的`上传文件`功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;可以在主函数中使用以下代码导入此库。

```python
from paj7620u2 import PAJ7620U2
```

### 类

#### PAJ7620U2(i2c, address)

- `i2c` — 已经构造好的`IIC`对象。
- `address` - `I²C`设备地址。

#### 构造PAJ7620U2

&emsp;&emsp;在对象构造函数时需要传入一个已经构造好的`IIC`对象以及`I²C`设备地址。

```python
i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000) # 构造IIC对象，将1号引脚设置为scl，将0号引脚设置为sda
address = i2c.scan()[0] # 扫描I²C地址
gesture = PAJ7620U2(i2c, address) # 构造手势传感器对象
```

### 函数

&emsp;&emsp;在接下来的示例中, 构造一个PAJ7620U2对象来列举其成员函数。

```python
from machine import I2C
from paj7620u2 import PAJ7620U2, delay

# 构造IIC对象，将1号引脚设置为scl，将0号引脚设置为sda
i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000)
address = i2c.scan()[0]

# 创建手势传感器对象
gesture = PAJ7620U2(i2c, address)
```

#### 读取寄存器数据

&emsp;&emsp;`gesture.read(register, length)`

&emsp;&emsp;函数说明：从PAJ7620U2的指定寄存器开始往后连续读取各寄存器数据并返回。

&emsp;&emsp;`register`: 操作的起始寄存器地址。

&emsp;&emsp;`length`: 读入的字节数。

#### 写入寄存器数据

&emsp;&emsp;`write(register, data)`

&emsp;&emsp;函数说明：向PAJ7620U2的寄存器写入数据，返回`True`或`False`表示是否写入成功。

&emsp;&emsp;`register`: 操作的起始寄存器地址。

&emsp;&emsp;`data`: 需要写入的数据。

#### 寄存器分块选择

&emsp;&emsp;`select_bank(bank)`

&emsp;&emsp;函数说明：在PAJ7620U2的寄存器中选择相应的分块。

&emsp;&emsp;`bank`: 寄存器分块编号，可以为`0`或`1`。

#### 传感器模式选择

&emsp;&emsp;`change_mode(mode)`

&emsp;&emsp;函数说明：选择传感器中几种不同的模式。

&emsp;&emsp;`mode`: 模式编号。可以为`gesture.gesture_mode`(手势模式)(`0`)、`gesture.proximity_mode`(接近模式)(`1`)、`gesture.suspend_mode`(挂起模式)(`2`)、`gesture.resume_mode`(恢复模式)(`3`)。

#### 手势检测开关

&emsp;&emsp;`gesture_switch(up=True, down=True, left=True, right=True, forward=True, backward=True, clockwise=True, counterclockwise=True, wave=True)`

&emsp;&emsp;函数说明：开关相应手势的检测，参数分别可以为`True`(打开)或`False`(关闭)，默认为`True`。

&emsp;&emsp;参数：

- `up` - 向上。

- `down` - 向下。

- `left` - 向左。

- `right` - 向右。

- `forward` - 向前。

- `backward` - 向后。

- `clockwise` - 顺时针。

- `counterclockwise` - 逆时针。

- `wave` - 摇摆。

#### 获取手势数据

&emsp;&emsp;`get_gesture()`

&emsp;&emsp;函数说明：返回传感器检测到的数据。

&emsp;&emsp;返回值：

- **手势模式**：

- `gesture.up` - 向上(`1`)。

- `gesture.down` - 向下(`2`)。

- `gesture.left` - 向左(`4`)。

- `gesture.right` - 向右(`8`)。

- `gesture.forward` - 向前(`16`)。

- `gesture.backward` - 向后(`32`)。

- `gesture.clockwise` - 顺时针(`64`)。

- `gesture.counterclockwise` - 逆时针(`128`)。

- `gesture.wave` - 摇摆(`256`)。

- **接近模式**：

- `gesture.leave` - 离开(`0`)。

- `gesture.approve` - 接近(`1`)。

<br>

**注：关于此库相关细节说明详见代码注释**

## 案例代码复现

&emsp;&emsp;可以获取[main.py](/PAJ7620U2_B/code/main.py)函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器并`烧录`至`Waffle Nano`，以复现此案例。

&emsp;&emsp;案例相关细节说明详见代码注释。