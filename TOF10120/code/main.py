import utime
from machine import SPI, Pin
import st7789
from tof10120 import uart_init,convert_data,screen_init
uart=uart_init()# 传感器初始化

display=screen_init()  # 屏幕初始化
# 开始计时
start = utime.ticks_us()  # 开始时间为微秒的递增计数器
x = uart.read()
x = uart.read()# 去除刚开始的不稳定因素
utime.sleep(0.1)
while True:
    x = uart.read()# 读取数据
    # print(x)
    x = str(x)
    x1=convert_data(x)# 获得处理后的字符串
    x2 = int(x1)
    x1 = x1 + 'mm'# 给字符串加上单位
    display.fill(st7789.color565(255, 192, 203))
    display.fill_rect(0, 0, 240, 120, st7789.color565(0, 255, 255))# 画实心矩形
    display.fill_circle(120, 180, 60, st7789.color565(189, 252, 201))# 画实心圆
    display.draw_string(80, 160, x1, size=3, color=st7789.color565(0, 0, 0))
    if x2 > 800:# 判断距离是否合适并给予提示
        display.draw_string(30, 60, 'too far', size=3, color=st7789.color565(0, 0, 0))
    elif x2 < 500:
        display.draw_string(30, 60, 'too close!', size=3, color=st7789.color565(255, 0, 0))
    else:
        display.draw_string(30, 60, 'perfect!', size=4, color=st7789.color565(0, 255, 0))

    utime.sleep(1)
    sum = utime.ticks_diff(utime.ticks_us(), start)  # 测量开始时间到新的微秒的递增计数器开始时的差值。
    sum /= 1000000
    if sum > 10:  # 半小时使用时间后提示
        display.fill(st7789.color565(0, 200, 0))  # 填充绿色进入休息时间
        display.draw_string(60, 60, 'Please', size=4, color=st7789.color565(255, 255, 255))
        display.draw_string(0, 150, 'take a break', size=4, color=st7789.color565(255, 255, 255))
        utime.sleep(10)  # 休息三分钟
        start = utime.ticks_us()  # 重置计时器
        continue









