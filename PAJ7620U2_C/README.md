
### 标题 PAJ7620U2手势识别测视力原型

## 案例展示

一个手势传感器，探测人的手势，并将其放到代码中对人的手势进行判断，以符号E的改变呈现出来。

![img3](https://gitee.com/xiao_hanyu/waffle-nano-v1-sensor-lib/raw/master/PAJ7620U2_C/img/3.jpg "img3")

图为识别过程中屏幕上呈现的内容



## 物理连接

### 传感器选择

传感器选择如下图所示的型号为PAJ7620U2的手势识别传感器模组

![img1](https://gitee.com/xiao_hanyu/waffle-nano-v1-sensor-lib/raw/master/PAJ7620U2_C/img/1.jpg "img1")

![img2](https://gitee.com/xiao_hanyu/waffle-nano-v1-sensor-lib/raw/master/PAJ7620U2_C/img/2.jpg "img2")

### 传感器接线

传感器与waffle Nano之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连状态。

| Waffle Nano | 传感器 |
| :---------- | ------ |
| 3.3         | VCC    |
| IO0         | SCL    |
| IO1         | SDA    |
| GND         | GND    |



## 传感器库使用

可以获取[paj7620u2.py](https://gitee.com/xiao_hanyu/waffle-nano-v1-sensor-lib/blob/master/PAJ7620U2_C/code/paj7620u2.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。 

我们在可以在主函数中使用以下代码导入此库。

```
from paj7620u2 import PAJ7620U2 
```

在对象构造函数中，我们需要传入一个已经构造好的`IIC`对象

```
# 此处省略IIC对象的构造过程
g=PAJ7620U2(i2c) #构造手势传感器的对象
```



## 案例代码复现

  可以获取[main.py](https://gitee.com/xiao_hanyu/waffle-nano-v1-sensor-lib/blob/master/PAJ7620U2_C/code/main.py)代码，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例，基础效果如下所示：

![img4](https://gitee.com/xiao_hanyu/waffle-nano-v1-sensor-lib/raw/master/PAJ7620U2_C/img/4.jpg "img4")

  案例相关细节说明详见代码注释

