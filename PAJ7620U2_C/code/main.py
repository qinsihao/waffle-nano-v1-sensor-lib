from paj7620u2 import PAJ7620U2
from machine import SPI, Pin, I2C       #导入通信以、引脚相关的库及IIC基本库
import st7789                           #导入屏幕驱动库
import utime
import urandom

i2c = I2C(1, scl=Pin(1), sda=Pin(0), freq=400000)#构造IIC基本对象
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))

eye = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
eye.init()
eye.fill(st7789.color565(245,245,247))


if __name__ == "__main__":                                         #抛入样例
    list1=[11,10,9,8.5,7.5,6.6,6,5,4,3,2,1.5,1]                    #各个视力等级对应的符号E大小比例
    list2=[4.0,4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,5.0,5.1,5.2]    #各个视力等级
    s=5                                                            #随便定义了数一遍更改种子
    urandom.seed(s)                                                #定义了一个随机数种子，为取不同的方向准备
    a = urandom.randint(1,4)                                       #表示屏幕上符号的方向
    print(a)

    i=0              #形参
    bili=list1[i]
    b=0              #表示读出的方向

    def fx(a):       #由前面的随机数在屏幕上给出相应方向的符号E
        if a == 1:                                                   #上
            eye.fill_rect(int(120-bili*10), int(120+bili*6), int(20*bili), int(bili*4), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*10), int(120-bili*10),int( bili*4), int(20*bili), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*2), int(120-bili*10),int(bili*4), int(20*bili), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120+bili*6), int(120-bili*10), int(bili*4), int(20*bili), st7789.color565(0, 0, 0))
        elif a == 2:                                                #下
            eye.fill_rect(int(120-bili*10),int(120-bili*10), int(20*bili),int(bili*4), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*10),int( 120-bili*10),int( bili*4), int(20*bili), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*2),int(120-bili*10),int(bili*4),int(20*bili), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120+bili*6),int(120-bili*10),int( bili*4),int(20*bili), st7789.color565(0, 0, 0))
        elif a == 4:                                                #左3
            eye.fill_rect(int(120-bili*10), int(120-bili*10), int(20*bili),int( bili*4), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120+bili*6), int(120-bili*10), int(bili*4), int(20*bili), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*10),int(120-bili*2), int(20*bili), int(bili*4), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*10), int(120+bili*6), int(20*bili),int(bili*4), st7789.color565(0, 0, 0))
        else:                                                       #右4
            eye.fill_rect(int(120-bili*10), int(120-bili*10), int(20*bili),int( bili*4), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*10), int(120-bili*10), int(bili*4),int(20*bili), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*10),int(120-bili*2),int(20*bili), int(bili*4), st7789.color565(0, 0, 0))
            eye.fill_rect(int(120-bili*10), int(120+bili*6),int(20*bili),int(bili*4), st7789.color565(0, 0, 0))


    g = PAJ7620U2(i2c)
    NoE = 0
    while True:

        if NoE>3: break
        if i>11: break

        utime.sleep(.5)#.1
        fx(a)

        data = g.PAJ7620U2ReadReg(0x43, 1)[0]
        print(data)
        if data:
            if data == g.GES_UP_FLAG:
                b=1
                print("Up")
                utime.sleep(0.5)
            elif data == g.GES_DOWN_FLAG:
                b=2
                print("Down")
                utime.sleep(0.5)
            elif data == g.GES_RIGHT_FLAG:
                b=3
                print("Right")
                utime.sleep(0.5)
            elif data == g.GES_LEFT_FLAG:
                b=4
                print("Left")
                utime.sleep(0.5)
            elif data == g.GES_FORWARD_FLAG:
                b=5
            elif data == g.GES_BACKWARD_FLAG:
                b=6
            elif data == g.GES_CLOCKWISE_FLAG:
                b=7
            elif data == g.GES_COUNT_CLOCKWISE_FLAG:
                b=8
            elif data == g.GES_WAVE_FLAG:
                b=9

            while (i<13):

                if(a==b):#若正确，屏幕上的比例会变小，随机数种子更新,重置背景防止图像重叠
                    bili=list1[i+1]
                    i=i+1
                    s=s+1
                    urandom.seed(s)
                    a = urandom.randint(1,4)
                    eye.fill(st7789.color565(255, 250, 250))
                    fx(a)
                    utime.sleep(1)
                    print("True")
                    break
                else:#错误次数+1，
                    print("False")
                    NoE=NoE+1
                    s=s+1
                    urandom.seed(s)
                    a = urandom.randint(1,4)
                    eye.fill(st7789.color565(255, 250, 250))
                    fx(a)
                    if(a==b):
                        NoE=NoE-1
                        break
                    else:
                        NoE=NoE+1
                        print ("Your vision is")
                        print (list2[i])
                        break

    #输出结果并显示在屏幕上
    print ("Your vision is")
    print (list2[i])
    eye.fill(st7789.color565(250,250,210))
    eye.draw_string(10, 20,"Your vision is", color=st7789.color565(0, 0, 0), size=2,bg=st7789.color565(250,250,210))
    eye.draw_string(80, 100,str(list2[i]), color=st7789.color565(0, 0, 0), size=5,bg=st7789.color565(250,250,210))
