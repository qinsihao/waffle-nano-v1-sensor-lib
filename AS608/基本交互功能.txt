部分核心功能的指令包：
注：缓冲区CharBuffer1、CharBuffer2的BufferID分别为x01和x02，如果指定其他值，按照CharBuffer2处理

（1）录入图像（探测手指，探测到后录入指纹存于ImageBuffer），返回确认码：
         xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x01\x00\x05

（2）生成特征（将ImageBuffer中的原始图像生成指纹特征文件存于CharBuffer1或CharBuffer2：
         xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x04\x02\（BufferID-1字节）\（校验和-2字节）

（3）精确比对CharBuffer1和CharBuffer2中的指纹特征：
         xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x03\x00\x07

（4）合并特征（将CharBuffer1与CharBuffer2中的特征文件合并生成模板，结果存于CharBuffer1与CharBuffer2）：
        xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x05\x00\x09

（5）将CharBuffer1或CharBuffer2中的模板文件存到PageID号flash数据库位置：
        xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x06\x06\（BufferID-1字节）\（位置号-2字节）\（校验和-2字节）

（6）清空指纹库（删除flash数据库中指纹模板）：
        xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x0d\x00\x11

（7）搜索指纹（自动采集指纹，在指纹库中搜索目标模板并返回搜索结果）
        xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x11\x00\x15

 