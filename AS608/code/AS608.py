from machine import UART, Pin, SPI
import st7789
import utime

def uart_init():
    uart = UART(1)
    uart.init(baudrate=57600, parity=None, tx=Pin(0), rx=Pin(5))  # 构造串口，启用uart通信协议
    return uart

def fingerprint1(display,uart):
    for i in range(0, 8):  # 指令（1）：录入指纹
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x01\x00\x05')
        utime.sleep(1)
        dd = uart.read()
        print(dd)#测试点
        if dd[9] == 0:  # 指纹成功录入
            display.draw_string(10, 50, "School success", size=3, color=st7789.color565(255, 255, 0), bg=st7789.color565(65, 105, 225))
            break
        if i == 7:  # 指纹没有录入成功
            display.draw_string(10, 50, "Entry failure", size=3, color=st7789.color565(255, 125, 64), bg=st7789.color565(65, 105, 225))
    utime.sleep(2)

def buffer1(display,uart):
    for i in range(0, 8):  # 指令（2）：指纹生成模板存于Buffer1
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x04\x02\x01\x00\x08')
        utime.sleep(1)
        aa = uart.read()
        print(aa)#测试点
        if aa[9] == 0:  # 成功上传到Buffer1
            display.draw_string(10, 80, "Successfully upload", size=2, color=st7789.color565(0, 255, 0), bg=st7789.color565(65, 105, 225))
            break
        if i == 7:  # 无法上传到Buffer1
            display.draw_string(10, 80, "Fail to upload", size=2, color=st7789.color565(255, 97, 3), bg=st7789.color565(65, 105, 225))
    utime.sleep(3)

def fingerprint2(display,uart):
    for i in range(0, 8):  # 指令（1）：录入指纹，第二次录入指纹
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x01\x00\x05')
        utime.sleep(1)
        dd = uart.read()
        print(dd)#测试点
        if dd[9] == 0:  # 指纹成功录入
            display.draw_string(10, 110, "School success", size=3, color=st7789.color565(255, 255, 0), bg=st7789.color565(65, 105, 225))
            break
        if i == 7:  # 指纹没有录入成功
            display.draw_string(10, 110, "Entry failure", size=3, color=st7789.color565(255, 125, 64), bg=st7789.color565(65, 105, 225))
    utime.sleep(2)

def buffer2(display,uart):
    for i in range(0, 8):  # 指令（2）：指纹生成模板存于Buffer2
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x04\x02\x02\x00\x09')
        utime.sleep(1)
        aa = uart.read()
        print(aa)
        if aa[9] == 0:  # 成功上传到Buffer2
            display.draw_string(10, 140, "Successfully upload", size=2, color=st7789.color565(0, 255, 0), bg=st7789.color565(65, 105, 225))
            break
        if i == 7:  # 无法上传到Buffer2
            display.draw_string(10, 140, "Fail to upload", size=2, color=st7789.color565(255, 97, 3), bg=st7789.color565(65, 105, 225))
    utime.sleep(2)

def compare(display,uart,str1):
    for i in range(0, 8):  # 指令（3）：对比两次录入指纹的质量
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x03\x00\x07')
        utime.sleep(1)
        aa = uart.read()
        print(aa)#测试点
        if aa[9] == 0:  # 对比成功，显示匹配度
            if aa[10] + aa[11] > 15:
                display.draw_string(10, 10, "It's a good match", size=2, color=st7789.color565(0, 255, 0), bg=st7789.color565(160, 32, 240))
                display.draw_string(10, 40, "Your match", size=3,color=st7789.color565(0, 0, 0), bg=st7789.color565(160, 32, 240))
                display.draw_string(10, 70, "score is ", size=3, color=st7789.color565(0, 0, 0), bg=st7789.color565(160, 32, 240))
                display.draw_string(10, 120, str(aa[10] + aa[11]), size=4,color=st7789.color565(255, 0, 0), bg=st7789.color565(160, 32, 240))
            else:# 对比成功，匹配度较低
                display.draw_string(10, 10, "The match is", size=2, color=st7789.color565(0, 0, 255), bg=st7789.color565(160, 32, 240))
                display.draw_string(10, 40, "a little bit bad", size=2, color=st7789.color565(0, 0, 255), bg=st7789.color565(160, 32, 240))
                display.draw_string(10, 70, "Your match", size=3, color=st7789.color565(0, 0, 0), bg=st7789.color565(160, 32, 240))
                display.draw_string(10, 100, "score is ", size=3, color=st7789.color565(0, 0, 0), bg=st7789.color565(160, 32, 240))
                display.draw_string(10, 150, str(aa[10] + aa[11]), size=4,color=st7789.color565(255, 0, 0), bg=st7789.color565(160, 32, 240))
            utime.sleep(2)

            for j in range(0, 8):  # 指令（5）：将Buffer1与Buffer2中的特征文件合并生成模板，结果存于Buffer1与Buffer2。
                uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x05\x00\x09')
                utime.sleep(1)
                bb = uart.read()
                print(bb)#测试点
                if bb[9] == 0:  # 传入成功
                    display.draw_string(10, 160, "Merger success", size=3, color=st7789.color565(0, 255, 0), bg=st7789.color565(160, 32, 240))
                    break
                if j == 7:  # 传入失败
                    display.draw_string(10, 160, "Merger failure", size=3, color=st7789.color565(227, 23, 13), bg=st7789.color565(160, 32, 240))
            utime.sleep(2)

            for i in range(0, 8):  # 指令（6）：将匹配好的指纹储存到指纹库指定位置
                uart.write(str1) #可以自己选择储存的位置，这边只是做一个案例
                utime.sleep(1)
                dd = uart.read()
                print(dd)#测试点
                if dd[9] == 0:#储存成功
                    display.draw_string(10, 190, "Store success", size=3, color=st7789.color565(0, 255, 0), bg=st7789.color565(160, 32, 240))
                    break
                if i == 7:#储没有成功
                    display.draw_string(10, 190, "Store failure", size=3, color=st7789.color565(227, 23, 13), bg=st7789.color565(160, 32, 240))
            utime.sleep(2)

            break
        if i == 7:  # 匹配没有成功，无法存入指纹库
            display.draw_string(25, 100, "Match failure", size=3, color=st7789.color565(255, 0, 0), bg=st7789.color565(160, 32, 240))
    utime.sleep(2)

def search(display,uart):
    for i in range(0, 8):  # 搜索指纹（现场自动匹配）
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x11\x00\x15')  # 可以自己选择储存的位置，这边只是做一个案例
        utime.sleep(1)
        dd = uart.read()
        print(dd)#测试点
        if dd[9] == 0:  # 用户搜索成功，返回用户ID以及匹配度
            if dd[12] + dd[13] > 15:
                display.draw_string(10, 10, "The search ", size=3, color=st7789.color565(0, 255, 0), bg=st7789.color565(255, 255, 0))
                display.draw_string(10, 60, "is a good match", size=3, color=st7789.color565(0, 255, 0), bg=st7789.color565(255, 255, 0))
                display.draw_string(10, 120, str(dd[10] + dd[11]) + ", Your match score is ", size=2,color=st7789.color565(0, 0, 0), bg=st7789.color565(255, 255, 0))
                display.draw_string(60, 170, str(dd[12] + dd[13]), size=4,color=st7789.color565(160, 32, 240), bg=st7789.color565(255, 255, 0))
            else:
                display.draw_string(10, 10, "Your match ", size=3, color=st7789.color565(255, 0, 0), bg=st7789.color565(255, 255, 0))
                display.draw_string(10, 60, "is not good", size=3, color=st7789.color565(255, 0, 0), bg=st7789.color565(255, 255, 0))
                display.draw_string(10, 120, str(dd[10] + dd[11]) + ", Your match score is ", size=2,color=st7789.color565(0, 0, 0), bg=st7789.color565(255, 255, 0))
                display.draw_string(60, 170, str(dd[12] + dd[13]), size=4, color=st7789.color565(160, 32, 240), bg=st7789.color565(255, 255, 0))
            break
        if i == 7:#搜索失败
            display.draw_string(25, 100, "Search failure", size=3, color=st7789.color565(255, 0 , 0), bg=st7789.color565(255, 255, 0))
    utime.sleep(2)

def clear(display,uart):
    for i in range(0, 8):  # 指令（13）:清空指纹库
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x0d\x00\x11')
        utime.sleep(1)
        aa = uart.read()
        print(aa)#测试点
        if aa[9] == 0:  #清空成功
            display.draw_string(25, 100, "Empty success", size=3, color=st7789.color565(160, 32, 240), bg=st7789.color565(255, 97, 0))
            break
        if i == 7:  #清空失败
            display.draw_string(25, 100, "Empty failure", size=3, color=st7789.color565(160, 32, 240), bg=st7789.color565(255, 97, 0))
    utime.sleep(2)

print(1)#判断库是否调用正确的一个测试点