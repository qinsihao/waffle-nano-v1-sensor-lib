from machine import SPI,Pin
import st7789,gesture_sensor,utime
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))                  
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.fill(st7789.color565(255, 255, 255))
while(1):
    gesture=gesture_sensor.direction()                              #调用gesture函数，直接返回手势的值
    display.draw_string(10, 100,'slip'+gesture,size=2,color=st7789.color565(0, 0, 0),bg=st7789.WHITE)
    utime.sleep_ms(200)
    display.fill(st7789.color565(255, 255, 255))