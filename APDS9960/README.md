# ADPS9960手势传感器

## 案例展示

​		ADPS9960是一个十分灵敏，功能繁多的传感器。本案例仅展示其识别手势的功能，除此之外它还能够测量温度，识别颜色等

​		下图中显示屏上的slip后的词可以根据你手相对于传感器的滑动的方向在 “up”,“down”,“left”,“right”之间不断变动

![](image/20210720174256.jpg)

## 物理连接

### 传感器选择

​		传感器选择如下图中的APDS9960手势传感器

![](image/20210720174312.jpg)

### 传感器接线

​		传感器一共有六个接脚，其中LV不需要连接，INT为中断接脚，如果不需要终端功能的话也可以不连，下图是剩余四个接脚与Waffle Nano接脚的连线表

| Waffle Nano | ADPS9960 |
| ----------- | -------- |
| 3V3         | VCC      |
| GND         | GND      |
| G00         | SDA      |
| G01         | SCL      |

## 传感器库使用

​		[在此](https://gitee.com/jy53181321/waffle-nano-v1-sensor-lib/blob/master/APDS9960/code/gesture_sensor.py)获取gesture_sensor.py（APDS9960库），将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到Waffle Nano上

​		使用方法为，先将库导入程序中

```python
import gesture_sensor
```

​		然后就可以直接用了（要注意先把屏幕初始化才能在屏幕上显示出方向）

```python
gesture=gesture_sensor.direction()
```

​		详细的可以看gesture_sensor.py

## 案例代码复现

​		可以[在此](https://gitee.com/jy53181321/waffle-nano-v1-sensor-lib/blob/master/APDS9960/code/main.py)获取main.py,将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给Waffle Nano,以复现此案例

(可能效果不会很好，由于水平所限，请见谅)