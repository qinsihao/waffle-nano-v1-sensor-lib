from machine import Pin
from machine import ADC
import utime
x=ADC(Pin(5))
y=ADC(Pin(12))
z=ADC(Pin(13))
x.equ(ADC.EQU_MODEL_8)
x.atten(ADC.CUR_BAIS_DEFAULT)
y.equ(ADC.EQU_MODEL_8)
y.atten(ADC.CUR_BAIS_DEFAULT)
z.equ(ADC.EQU_MODEL_8)
z.atten(ADC.CUR_BAIS_DEFAULT)

def xyzread(x,y,z):#读取xyz三轴加速度并以列表返回
    accx=x.read()
    accy=y.read()
    accz=z.read()
    acclist=[accx,accy,accz]
    return acclist
def xread(x):#读取x轴加速度
    accx=x.read()
    return accx
def yread(y):#读取y轴加速度
    accy=y.read()
    return accy
def zread(z):#读取z轴加速度
    accz=z.read()
    return accz
