from machine import ADC, Pin
import utime


class JoystickShield:
    def __init__(self, pinA, pinB, pinC, pinD, adc1, adc2):
        # 从GPIO接口得到按键A、B、C、D的信号
        self.pinA = pinA
        self.pinB = pinB
        self.pinC = pinC
        self.pinD = pinD
        # 从ADC接口得到摇杆X、Y轴的信号
        self.adc1 = adc1
        self.adc2 = adc2
        utime.sleep(0.1)

    def read(self):
        A = self.pinA.value()
        B = self.pinB.value()
        C = self.pinC.value()
        D = self.pinD.value()
        self.adc1.equ(ADC.EQU_MODEL_8)
        X = self.adc1.read()
        self.adc2.equ(ADC.EQU_MODEL_8)
        Y = self.adc2.read()
        # 数据加工，将大于摇杆未动的稳定值的数值记为1（正方向移动），反之记为-1。
        if X < 850:
            X = -1
        elif X > 1050:
            X = 1
        else:
            X = 0
        if Y < 850:
            Y = -1
        elif Y > 1050:
            Y = 1
        else:
            Y = 0
        # 在返回的数组中，按钮：1未按下，0按下；摇杆：0不动，-1负方向，1正方向
        data = [A, B, C, D, X, Y]
        return data