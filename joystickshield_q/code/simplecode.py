from joystickshield import JoystickShield
from machine import SPI, Pin, ADC
import utime
import st7789
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
# 全局变量
link = False
background_color = st7789.color565(255, 255, 255)        #白色
text_color = color=st7789.color565(0, 0, 0)             #黑色
shoot_color = st7789.color565(255, 0, 0)                 #红色
pao_color = st7789.color565(255, 255, 0)                #黄色
enemy_color = st7789.color565(255, 0, 255)              #紫色
line_color = st7789.color565(0, 0, 255)                     #蓝色
x_line = 120
y_line= 0
mylen = 12                                          #划分为20*20区域
# 初始化屏幕
screen = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
screen.init()
screen.draw_string(50, 230, "Powered by BlackWalnuut Labs.")
screen.fill(background_color)
# 实例化JoystickShield对象
pinA = Pin(14,Pin.IN)                                               #设置14号引脚为按钮A
pinB = Pin(0,Pin.IN)                                                #设置0号引脚为按钮B
pinC = Pin(1,Pin.IN)                                                #设置1号引脚为按钮C
pinD = Pin(2,Pin.IN)                                                #设置0号引脚为按钮D
adcX = ADC(Pin(12))                                                 #设置12号引脚为摇杆X轴
adcY = ADC(Pin(13))                                                 #设置13号引脚为摇杆Y轴
joystick = JoystickShield(pinA, pinB, pinC, pinD, adcX, adcY)
# 由于先开板子再开摇杆，会出现一段无连接时间
Nowdata = joystick.read()
while not([1, 1, 1, 1, 0, 0] == Nowdata):
    screen.draw_string(30, 100, "No signal", size=4, color = text_color)
    utime.sleep(0.5)
    Nowdata = joystick.read()
lianJie = True
if lianJie == True:
    screen.fill(st7789.color565(0, 0, 0))
    screen.draw_string(30, 100, "Loading...", size=4, color = line_color)
    utime.sleep(1)
# 使用摇杆控制瞄准线在屏幕上移动
screen.fill(background_color)
screen.fill_circle(120, 240, mylen, pao_color)
screen.line(120, 240, x_line, y_line, line_color)
while True:
    Nowdata = joystick.read()
    if Nowdata[-1] == 1:                                            # 向上移动,到边界了则不移动
        screen.line(120, 240, x_line, y_line, background_color)
        if x_line==0 or x_line==240:
            y_line -= 2
    elif Nowdata[-1] == -1:                                         # 向下移动,到边界了则移动，否则不移动
        screen.line(120, 240, x_line, y_line, background_color)
        if x_line==0 or x_line==240:
            y_line += 2
    elif Nowdata[4] == -1:                                          # 向左移动,到边界了则向下移动
        screen.line(120, 240, x_line, y_line, background_color)
        if x_line==0 or x_line==240:
            y_line += 2
        else:
            x_line -= 2
    elif Nowdata[4] == 1:                                           # 向右移动,到边界了则向下移动
        screen.line(120, 240, x_line, y_line, background_color)
        if x_line==0 or x_line==240:
            y_line += 2
        else:
            x_line += 2
    screen.line(120, 240, x_line, y_line, line_color)
    screen.fill_circle(120, 240, mylen, pao_color)
    utime.sleep(0.1)                                                # 最小操作间隔为0.1s