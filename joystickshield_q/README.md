#  Joystick Shield游戏摇杆扩展板

### 传感器

   如下图所示的型号为Joystick Shield的游戏摇杆扩展板模组。

![img](image\实物图.jpg)

 因Joystick Shield的游戏摇杆扩展板的电源有3.3V和5V两个选项和切换3.3V和5V的开关，但WaFFle nano只能对外提供3.3V电压，因此我们将切换3.3V/5V的部分作为传感器的开关。

### 传感器示意图

 注：传感器引脚上并无标签，如果尝试连线，请参照下图或咨询卖家（遥感引脚左X右Y）。

![连接示样](image\连接示样.webp)

## 传感器库使用

  该传感器在WaFFle中的库为 [joystickshield.py](code\joystickshield.py) ,下载此库，并通过文件上传功能将此库上传到`Waffle Nano`板子上，即可引用。

 引用该库的代码为

```
from joystickshield import JoystickShield
```

 想要实例化一个`JoystickShield`对象`joystick`，需要4个GPIO引脚，两个ADC引脚。构造方法如下

```
# 引脚引用过程省略

joystick = JoystickShield(pinA, pinB, pinC, pinD, adcX, adcY)
```

 `joystick`对象的`read()`方法返回的是一个有六个元素的数组。分别代表ABCD和摇杆XY，具体位置详见注释。

```
Nowdata = joystick.read()
```

## 案例复现

 本案例是一个简单的，使用摇杆和按键操控细线在20*20的格子上移动并显示在屏幕上的小游戏。

![展示](image\展示.jpg)

 首先，当开关未开启，会出现“Please connect”的提示。

![无信号](image\无信号.jpg)

 当开关开启后，会先提示“Loading...”，然后在1s后正式进入游戏界面。初始位置为屏幕下方中央（移动摇杆后出现细线）。

![连接成功](image\连接成功.jpg)

![开始图](image\开始图.jpg)

 可以通过摇杆的左右晃动操控细线移动。

![展示2](image\展示2.jpg)

  案例介绍到此结束，可以获取案例代码  [simplecode.py](code\simplecode.py)  ，在已经上传过库文件 [joystickshield.py](code\joystickshield.py) 的情况下将其内容复制到网页编译器上烧录到`Waffle Nano`，可复现此案例。

  库文件代码中的注释有部分数据处理方法解释和对返回值的解读。

