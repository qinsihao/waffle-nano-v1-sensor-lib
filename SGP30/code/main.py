from machine import I2C,Pin,SPI  #导入通信以及引脚相关的库
from sgp30 import SGP30  #导入SGP30库
import utime
import st7789  #导入屏幕驱动库
i2c=I2C(1,sda=Pin(0),scl=Pin(1),freq=100000)  #构造IIC对象
yq=SGP30(i2c)  #构造甲醛测量仪

spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8)) #构造spi对象
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT)) #构造屏幕控制对象
display.init() #屏幕初始化

background_clr = st7789.color565(255, 255, 255) #设置背景颜色为白色

while True:
    (data1,data2)=yq.read()  #从甲醛测量仪中获取数据
    co2data='CO2: '+str(data1)+' ppm  '  
    tvocdata='TVOC: '+str(data2)+' ppb'
    display.fill(background_clr)  #背景屏幕颜色填充
    display.draw_string(20, 50,co2data, color=st7789.BLACK, bg=st7789.WHITE, size=3, vertical=False, rotate=st7789.ROTATE_0)
    display.draw_string(20, 100,tvocdata, color=st7789.BLACK, bg=st7789.WHITE, size=3, vertical=False, rotate=st7789.ROTATE_0)
